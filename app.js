const appid = 'wx99a9b869ca240531';

const zhr_domain = 'https://mdzzmdzzmdzz.tunnel.phpor.me';
const dev_domain = 'http://mditu.kingdeeresearch.com';
const dis_domain = 'http://m.shangmiyun.com/api/';

var util = require ('./utils/util.js'); 

//app.js
App({
  onLaunch: function () {
    // var logs = wx.getStorageSync('logs') || [];
    // logs.unshift(Date.now());
    // wx.setStorageSync('logs', logs);

  },
  login: function (params) {
    let that = this;
    let url = this.domain() + '/' + 'user/login'
    let data = util.jsonToForm(params)
    wx.login({
      success: function (res) {
        if (res.code) {
          wx.request({
            url: url,
            data: data,
            method: 'POST',
            header: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            success: function (res) {
              that.getNewUserInfo();
              let token = res.data.data.token;
              wx.setStorageSync({
                key: 'token',
                data: token
              })
            }
          })
        }
      }
    })
  },
  getUserInfo: function () {
    return this.globalData.userInfo;
  },
  getNewUserInfo: function () {
    let that = this;
    wx.getUserInfo({
      success: function (res) {
        let simpleUserInfo = res.userInfo;
        let url = that.domain() + '/' + '';
        wx.request({
            url: url,
            data: simpleUserInfo,
            method: 'POST',
            header: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
          success: function(res){
            let userInfo = res.data.data.userInfo;
            that.globalData.userInfo = userInfo;
          }
        })

        return userInfo; 
      }
    })
  },
  globalData: {
    userInfo: null
  },
  domain: function () {
    // return zhr_domain;
    // return dev_domain;
    return dis_domain;
  }
})