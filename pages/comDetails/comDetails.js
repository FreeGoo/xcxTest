/**
 * Created by kingdee on 2016/11/24.
 */
var util = require('../../utils/util.js'),
    app = getApp();

Page({
  data:{},

  // 点击地址进入地图
  gotoMap:function(){
    var that = this;

    wx.openLocation({
      latitude:that.data.lat,
      longitude:that.data.lng,
      name:that.data.com_name
    })
  },

  redirect:function(e){

    var that = this,
        para = e.currentTarget.dataset.parameter;
    wx.navigateTo({
      url: '../' + para + '/' + para + '?id=' + that.data.id
    })
  },

  onLoad:function(option){
    var id=option.id,
        that = this;

    wx.request({
      url: app.domain()+'/companyInfo/'+id,
      //   data: {},
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      // header: {}, // 设置请求的 header
      success: function(res){
        if(+res.data.errcode === 0){
          var data = res.data.data;
          that.setData({
            id:id,
            com_name:data.name,
            com_type:data.companyType,
            com_status:data.registerStatus,
            dutyPerson:data.dutyPerson,
            registerMoney:data.registerMoney,
            registerDate:data.registerDate?util.formatDate(data.registerDate):'暂无',
            browseNum:data.pvSum,
            favorNum:data.collectionSum,
            commentNum:data.commentSum,
            phoneNo:data.phone,
            emailNo:data.email,
            address:data.address,
            lat:data.location.lat,
            lng:data.location.lng
          })
        }

      }
    })
  }
})