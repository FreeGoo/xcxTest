var app = getApp();
Page({
    data:{},

    onLoad:function(option){
        var id = option.id,
            that = this;
        wx.request({
          url: app.domain()+'/company/tag/'+id,
          method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
          // header: {}, // 设置请求的 header
          success: function(res){       
              if(+res.data.errcode === 0){
                  that.setData({
                      tagList:res.data.data
                  })
              }
          }         
        })
    }
})