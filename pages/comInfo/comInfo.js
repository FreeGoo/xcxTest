// pages/comInfo/comInfo.js
var util = require('../../utils/util.js'),
    app = getApp();

Page({
  data:{},
  onLoad:function(options){
    var that = this,
        listArr = [],
        id = options.id;
    wx.request({
      url: app.domain()+'/company/shareholder/'+id,
      // data: {},
      method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      // header: {}, // 设置请求的 header
      success: function(res){
        if(+res.data.errcode === 0){
            for (var item of res.data.data){
              listArr.push({
                name:item.name,
                types:item.type === null?'-- --':item.type,
                paid:item.paid === null?'-- --':item.paid+'.00万元人民币',
                paidDate:item.paidDate === null?'-- --':util.formatDate(item.paidDate,'yyyy年MM月dd日'),
                subscribed:item.subscribed === null?'-- --':item.subscribed+'.00万元人民币',
                subscribedDate:item.subscribedData === null?'-- --':util.formatDate(item.subscribedDate,'yyyy年MM月dd日')
              })
            }
            that.setData({
                shareholderInfo:listArr
            })
        }
      }
    })
  }
})