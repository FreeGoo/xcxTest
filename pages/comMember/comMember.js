var app = getApp();
Page({
    data:{},
    onLoad:function(option){
        var that = this,
            id = option.id;
        
        wx.request({
          url: app.domain()+'/company/keyPerson/'+id,
          method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
          // header: {}, // 设置请求的 header
          success: function(res){
            // success
            if(+res.data.errcode === 0){
                that.setData({
                   memList:res.data.data 
                })                
            }
          }
        })
    }
})