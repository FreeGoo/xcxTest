var util = require('../../../utils/util.js');
var app = getApp();
const detailUrl = '../../comDetails/comDetails';
const listPath = 'collectCompany/show/list';
const addPath = 'collectCompany/add';
const cancelPath = 'collectCompany/cancel';


Page({
    data: {
        dataList: [
            {
                // companyId:'',
                // companyName:'',
                // @select
            }
        ],
        title: '我的收藏',
        offset: 0,
        limit: 20,
        localData: [
            {
                'companyId': '57e558ff1b6cc20c712c133e',
                'companyName': '深圳万物生长影视传媒有限公司',
            },
            {
                'companyId': '5786f8532444f5eff0200619',
                'companyName': '中国银行股份有限公司',
            }
        ]
    },
    onLoad: function (option) {
        this.fetchData('refresh');
    },
    onPullDownRefresh: function () {
        this.fetchData('refresh');
    },
    onReachBottom: function () {
        that.offset = that.offset + limit;
        this.fetchData('load');
    },
    fetchData: function (updateType) {
        let that = this;
        let url = app.domain() + '/' + listPath;
        let data = util.jsonToForm({
            'offset': that.offset,
            'limit': that.limit
        });
        wx.request({
            url: url,
            header: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: data,
            method: 'POST',
            success: function (res) {
                console.log(res);
                let data = res.data.data;
                if (updateType == 'refresh') {
                    // var dataList = data.datas;
                    var dataList = that.data.localData;
                } else if (updateType == 'load') {
                    // var dataList = dataList.concat(data.datas);
                    var dataList = dataList.concat(that.data.localData);
                }

                dataList.forEach(function (e) {
                    e.select = true;
                })
                that.setData({
                    dataList: dataList
                });
            }
        })
    },
    toDetail: function (e) {
        let id = e.currentTarget.id;
        let url = util.fillUrlWithJson(detailUrl, {
            'id': id
        });
        wx.navigateTo({
            url: url,
        })
    },
    changeStatus: function (e) {
        let that = this;
        let id = e.currentTarget.id;
        let index = e.currentTarget.dataset.index;
        let select = that.data.dataList[index].select;
        if (select == true) {
            var url = app.domain() + '/' + cancelPath;
            var title = '已取消收藏';
        } else {
            var url = app.domain() + '/' + addPath;
            var title = '收藏成功';
        }
        wx.request({
            url: url,
            data: {
                'companyId': id
            },
            success: function (res) {
                let dataList = that.data.dataList;
                dataList[index].select = !select;
                that.setData({ dataList: dataList });
                wx.showToast({
                    title: title,
                    icon: 'success',
                    duration: 1000
                })
            }
        })
    }
})