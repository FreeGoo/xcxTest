var app = getApp();
Page({
  data: {
    user: {},
    login: false,
    list: [
      {
        iconName: 'icon-star-collection',
        dataVal: 'comCollect',
        item_title: '我收藏的公司',
        url: './myCollection/myCollection'
      },
      {
        iconName: 'icon-clock',
        dataVal: 'comInform',
        item_title: '我的定制提醒',
        url: './myRemind/myRemind'
      },
      {
        iconName: 'icon-document',
        dataVal: 'comRecord',
        item_title: '我的查看记录',
        url: './myDocument/myDocument'
      },
      {
        iconName: 'icon-favorite',
        dataVal: 'comFavor',
        item_title: '我想要了解的公司',
        url: './myDocument/myDocument'
      }
    ],

  },
  //eventListener
  toDetails: function (param) {
    let that = this;
    let index = param.currentTarget.dataset.index;
    let url = that.data.list[index].url;
      wx.navigateTo({
        url: url
      })
  },

  // onLoad: function () {
  // var that = this
  // //调用应用实例的方法获取全局数据
  // app.getUserInfo(function(userInfo){
  //   //更新数据
  //   that.setData({
  //     user:userInfo
  //   })
  // });
  //   }
  login: function () {
    wx.navigateTo({
      url: '../login/login'
    })
  },

  onLoad: function () {
    var that = this;
    wx.request({
      url: app.domain() + '/user/loginStatus',
      // data: {},
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      // header: {}, // 设置请求的 header
      success: function (res) {
        if (+res.data.errcode === 401) {
          that.setData({
            login: false
          });
        } else if (+res.data.errcode === 0) {
          that.setData({
            login: true
          });
        }

      },
      fail: function () {
      },
      complete: function () {
        // complete
      }
    })

  }
})