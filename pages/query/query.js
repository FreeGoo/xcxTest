Page({
    data: {
        longitude: '',
        latitude: '',
        scale: '16',
        markers: [
            {
                latitude: 22.53943158,
                longitude: 113.9529403211,
                name: '1',
                desc: '11'
            },
            {
                latitude: 22.53943158,
                longitude: 113.9529403213,
                name: '2',
                desc: '22'
            }
        ],
        covers: [
            {
                latitude: 22.53943158,
                longitude: 113.9529403215,
                iconPath: '../images/car.png',
                rotate: 10
            }, {
                latitude: 22.53943158,
                longitude: 113.9529403217,
                iconPath: '../images/car.png',
                rotate: 90
            }
        ]
    },

    // 点击输入框跳转页面
    toSearchPage:function(){
        wx.navigateTo({
          url: '../search/search'
        })
    },

    onLoad: function (options) {
        console.log(options);
        var that = this;


        let markers = [
            {
                latitude: 22.53943158,
                longitude: 113.9529403215,
                name: '1',
                desc: '11'
            },
            {
                latitude: 22.53943158,
                longitude: 113.9529403217,
                name: '2',
                desc: '22'
            }
        ]
        
        let covers = [
            {
                latitude: 22.53943158,
                longitude: 113.9529403215,
                iconPath: '../../resources/images/query_sel.png',
                rotate: 0
            }, {
                latitude: 22.53943158,
                longitude: 113.9529403217,
                iconPath: '../../resources/images/query_sel.png',
                rotate: 0
            }
        ];



        wx.getLocation({
            type: 'wgs84',
            success: function (res) {
                console.log(res);
                var latitude = res.latitude
                var longitude = res.longitude
                that.setData({
                    longitude: longitude,
                    latitude: latitude,
                    markers: markers
                    // covers:covers
                });
            },
            fail: function () {
                console.log("fail");
            },
            complete: function () {
                console.log("complete");
            }
        })
    }
})
