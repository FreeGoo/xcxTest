var util = require('../../utils/util.js'),
    app = getApp();

Page({
    data:{
        loading:true,
        hasData:true,
        status:false,
        isEnd:false,
        offset:0,
        resultList: []
    },

    //跳转到公司详情页 
    toComDetails:function(e){
        var itemId = e.currentTarget.dataset.id;
        wx.navigateTo({
          url: '../comDetails/comDetails?id='+itemId
        })
    },

    // 滚动条事件
    scrollEvent:function(e){
        var that = this;
        this.setData({
            offset:that.data.offset+20,
            status:true
        })
        this.wxRequest();
    },

    // 接口请求
    wxRequest:function(){
        var that = this;
        wx.request({
          url: app.domain()+'/search',
        //   data:that.data.dataStr+'&offset='+that.data.offset,
          data:'offset='+that.data.offset+that.data.dataStr,
        //   data:data,
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
          success: function(res){
            that.setData({
                loading:false
            })
            if(+res.data.errcode===0){
                if(res.data.data.length !== 0){
                    var list = [],
                    data = res.data.data,
                    array = data.datas,
                    count = data.count;
               
                    that.setData({                        
                        hasData:true,
                        count:count,
                        resultList:that.data.resultList.concat(array),
                        status:false
                    }) 
                    
                }else{                    
                    that.setData({
                        isEnd:true,
                        status:false

                    })
                    console.log(that.data.isEnd);
                    console.log(that.data.status);

                }                  
                     
            }
          }          
        })
        
    },

    onLoad:function(option){  
        var searchKey = option.key,
            searchType = option.type,
            latitude = option.lat,
            longitude = option.lng,
            // data={
            //     offset:0,
            //     limit:20,
            //     searchKey:searchKey,
            //     searchType:searchType,
            //     lat:latitude,
            //     lng:longitude,
            //     zoom:16
            // };
            dataStr = '&limit=20&searchKey='+encodeURIComponent(searchKey)+'&searchType='+encodeURIComponent(searchType)+'&lat='+latitude+'&lng='+longitude+'&zoom=16';     
   
        if(option.industry !== undefined){
            dataStr += '&industry'+encodeURIComponent(option.industry);
            // data.industry = option.industry
        };

        if(option.regMoney !== undefined){
            dataStr += '&regMoney'+encodeURIComponent(option.regMoney);
            // data.regMoney = option.regMoney
        };

        if(option.regTime !== undefined){
            dataStr += '&regTime'+encodeURIComponent(option.regTime);
            // data.regTime = option.regTime
        };

        this.setData({
            key:searchKey,
            dataStr:dataStr
        }); 

        this.wxRequest();      

    }
})