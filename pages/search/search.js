Page({
    data:{        
        num:0,
        searchVal:'',
        careerVal:'',
        fundVal:'',
        dateVal:'',
        latitude:'',
        longitude:'',
        title:'公司',
        titleList:['公司','地址','人名'],
        careerList:[{name:'IT/互联网'},{name:'制造加工'},{name:'房产建筑'},{name:'金融投资'},{name:'广告传媒'},{name:'贸易物流'},{name:'医疗行业'},{name:'消费零售'},{name:'其他'}],
        fundList:[{name:'100万以下'},{name:'100~500万'},{name:'500~1000万'},{name:'1000~2000万'},{name:'2000万以上'},{name:'不限'}],
        dateList:[{name:'最近30天'},{name:'最近1年'},{name:'最近1~3年'},{name:'3年以上'}]
    },

    //控制下拉框显示隐藏 
    showList:function(){  
        var that= this;
        this.setData({
            num:that.data.num+1
        }) 
    },

    // 点击下拉框内容
    changeVal:function(e){        
        this.setData({
            title:e.target.dataset.item,
            num:0
        });
    },

    // 获取输入框内容
    getInputVal:function(e){
        this.setData({
            searchVal : e.detail.value
        })
    },  

    // 获取点击按钮的值和状态
    active:function(array,item,index,valName,val){  
        var obj = {};
        var isChoose = array[index].isChoose;
        
        if(isChoose === 1){
            obj[item+'['+index+'].isChoose'] = 0;
            val = '';
        } else{            
            for(var ind in array){
                obj[item+'['+ind+'].isChoose'] = 0;
            }
            obj[item+'['+index+'].isChoose'] = 1;
        }
        obj[valName] = val;
        this.setData(obj);
    }, 

    //获取行业分类
    getCareerVal:function(e){
        var index = e.currentTarget.dataset.id,
            value = e.target.dataset.value;
        this.active(this.data.careerList,'careerList',index,'careerVal',value);
    },

    //获取注册资金 
    getFundVal:function(e){
        var index = e.currentTarget.dataset.id,
            value = e.target.dataset.value;
        this.active(this.data.fundList,'fundList',index,'fundVal',value);
              
    },

    // 获取成立时间
    getDateVal:function(e){
        var index = e.currentTarget.dataset.id,
            value = e.currentTarget.dataset.value;
        this.active(this.data.dateList,'dateList',index,'dateVal',value);            
    },    

    search:function(){
        var url = '../result/result?type='+this.data.title+'&key='+this.data.searchVal+'&lat='+this.data.latitude+'&lng='+this.data.longitude;
        
        // 判断行业分类是否为空
        if(this.data.careerVal !== ''){
            url+='&industry='+this.data.careerVal;
        }
        
        // 判断注册资金是否为空
        if(this.data.fundVal !== ''){
            url+='&regMoney='+this.data.fundVal;
        }

        // 判断成立时间是否为空
        if(this.data.dateVal !== ''){
            url+='&regTime='+this.data.dateVal;
        }
        
        if(this.data.searchVal !== ''){
            wx.navigateTo({
                url: url                
            });

            this.setData({
                searchVal : '',
                careerVal : '',
                fundVal : '',
                dateVal : ''
            })             
        } else {
            return;
        }   
    },
    onLoad:function(){
        var that = this;
        wx.getLocation({
            success: function(res) {
                that.setData({
                    latitude : res.latitude,
                    longitude : res.longitude
                })                
            }
        });        
    }
})