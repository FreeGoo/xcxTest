var app = getApp();
var util = require('../../../utils/util.js');
const path = '/qianhai';
const detailUrl = '../../comDetails/comDetails';

Page({
    data: {
        dataList: [
            // {
            //     id: '',
            //     name: '',
            //     caption: '',
            // }
        ],
        moduleList: [
            {
                ptype: '注册资金top10',
                name: '注册资金排行',
                select: true,
                style: 'yellow',
                triangle_style: 'triangle-down-yellow'
            }, {
                ptype: '互联网金融top10',
                name: '互联网金融排行',
                select: false,
                style: 'blue',
                triangle_style: 'triangle-down-blue'
            }, {
                ptype: '奇葩名字top10',
                name: '奇葩名字大盘点',
                select: false,
                style: 'orange',
                triangle_style: 'triangle-down-orange'
            }, {
                ptype: '跨境电商top10',
                name: '跨境电商哪家强',
                select: false,
                style: 'green',
                triangle_style: 'triangle-down-green'
            }
        ],
        hidden: true
    },
    onReady: function () {
        let ptype = '注册资金top10';
        this.fetchData(ptype);
    },
    fetchData: function (ptype) {
        let that = this;
        let url = app.domain() + path;
        wx.request({
            url: url,
            data: { 'type': ptype },
            method: 'GET',
            success: function (res) {
                console.log(res);
                let dataList = res.data.data;
                dataList = fillData(dataList),
                that.setData({
                    dataList: dataList,
                    hidden: false
                });
                console.log(that.data.dataList);
            }
        })

    },
    switchModule: function (e) {
        let that = this;
        let ptype = e.currentTarget.dataset.ptype;
        let index = e.currentTarget.id;
        let moduleList = that.changeSelect(that.data.moduleList,index);
        that.setData({
            moduleList: moduleList,
            hidden: true
        });
        this.fetchData(ptype);
    },
    changeSelect: function(list,index){
        for(let i = 0;i < list.length;i++){
            let temp = list[i];
            temp.select = (i == index)? true:false;
            list[i] = temp;
        }
        return list;
    },
    toDetail: function (e) {
        console.log(e);
        let that = this;
        let id = e.currentTarget.id;
        let url = util.fillUrlWithJson(detailUrl,{'id': id});
        wx.navigateTo({
            url: url
        })
    }
})

function fillData(dataList) {
    dataList.forEach(function (e) {
        if (e.name == null) { e.name = '暂无'; }
        if (e.caption == null) { e.caption = '暂无'; }
        
    })
    return dataList;
}