var util = require('../../utils/util.js');
var app = getApp();

Page({
    data: {
        dataList: [
            {
                icon: 'icon-building',
                title: '前海企业',
                subTitle: '深圳前海商家聚集',
                url: './qianhai/qianhai'
            }, {
                icon: 'icon-star-company',
                title: '明星企业',
                subTitle: '寻找偶像的后花园',
                url: './star/star'
            }, {
                icon: 'icon-top500',
                title: '500强企业排行榜',
                subTitle: '各路枭雄一眼扫尽',
                url: './top500/top500'
            }, {
                icon: 'icon-web-star',
                title: '网红企业',
                subTitle: '深入挖掘最爱网红',
                url: './vote/vote'
            }
        ]
    },
    toDetail: function (e) {
        console.log(e);
        let that = this;
        let id = e.currentTarget.id;
        if (id == 1) {
            var url = util.fillUrlWithJson(that.data.dataList[id].url, {
                'title': '明星列表',
                'url': app.domain() + '/starCompany'
            });
        } else if (id == 3){
            let ptype = wx.getStorageSync('ptype').toString();
            var url =  ptype == null || ptype == ''? that.data.dataList[id].url:'./webstar/webstar';
        } else {
            var url = that.data.dataList[id].url;
        }
        wx.navigateTo({
            url: url
        })
    }
})