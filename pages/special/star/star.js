var app = getApp();
var util = require('../../../utils/util.js');
const detailPath = '../starDetail/starDetail';
const fromPath1 = '/starCompany/starInfo';
const fromPath2 = '/webstar-detail';

Page({
    data: {
        dataObjects: '',    
        // {A: [{id:'',name:''}]}
        letterList: [
            { letter: 'A' },
            { letter: 'B' },
            { letter: 'C' },
            { letter: 'D' },
            { letter: 'E' },
            { letter: 'F' },
            { letter: 'G' },
            { letter: 'H' },
            { letter: 'I' },
            { letter: 'J' },
            { letter: 'K' },
            { letter: 'L' },
            { letter: 'M' },
            { letter: 'N' },
            { letter: 'O' },
            { letter: 'P' },
            { letter: 'Q' },
            { letter: 'R' },
            { letter: 'S' },
            { letter: 'T' },
            { letter: 'U' },
            { letter: 'V' },
            { letter: 'W' },
            { letter: 'X' },
            { letter: 'Y' },
            { letter: 'Z' }
        ],
        hidden: false,
        toView: '',
        title: '',
        url: ''
    },
    onLoad: function(option){
        let title = option.title;
        let url = option.url;
        this.setData({
            title: title,
            url: url
        })
    },
    onReady: function () {
        this.fetchData();
    },
    fetchData: function () {
        let that = this;
        wx.request({
            url: that.data.url,
            method: 'GET',
            success: function (res) {
                console.log(res);
                let dataObjects = res.data.data;
                that.setData({
                    dataObjects: dataObjects
                });
                console.log(that.data.dataList);
            }
        })
    },
    switchLetter: function (e) {
        let letter = e.currentTarget.dataset.letter;
        this.setData({
            toView: letter
        })
        wx.showToast({title: letter});
    },
    toDetail: function (e) {
        let index = e.currentTarget.id;
        let letter = e.currentTarget.dataset.letter;
        let title = e.currentTarget.dataset.title;
        let id = this.data.dataObjects[letter][index].id;
        if(title == '明星列表'){
            var fromPath = fromPath1;
        }else if(title == '网红列表'){
            var fromPath = fromPath2;
        }else{
            console.log();
        }
        let url = util.fillUrlWithJson(detailPath,{'id': id,'fromPath': fromPath});
        wx.navigateTo({
            url: url
        })
    }
})

