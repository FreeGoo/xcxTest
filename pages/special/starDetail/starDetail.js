var app = getApp();
var util = require('../../../utils/util.js');
const baidu_static_url = 'http://api.map.baidu.com/staticimage/v2';
const detailUrl = '../../comDetails/comDetails';

Page({
    data: {
        dataList: [
            // {
            //     id: '',
            //     name: '',
            //     address: '',
            //     companyType: '',
            //     dutyPerson: '',
            //     registerMoney: '',
            //     registerAuthority: '',
            //     location: {
            //         "lng": '',
            //         "lat": ''
            //     },
            //     +imageUrl:''
            // }
        ]
        ,
        indexList: [
        ],
        hidden: false,
        name: '',
        description: ''
    },
    onLoad: function (option) {
        let id = option.id;
        let url = app.domain() + option.fromPath + '/' + id;
        if (option.fromPath == '/starCompany/starInfo') {
            this.fetchData(url, 'POST');
        } else {
            this.fetchData(url, 'GET');
        }
    },
    fetchData: function (url, method) {
        let that = this;
        wx.request({
            url: url,
            method: method,
            success: function (res) {
                console.log(res);
                let data = res.data.data;
                let name = data.name;
                let description = data.description;
                let dataList = data.starCompanies;
                dataList = fillData(dataList);
                that.setData({
                    name: name,
                    description: description,
                    dataList: dataList
                });
                console.log(that.data.dataList);
            }
        })
    },
    toDetail: function (e) {
        console.log(e);
        let that = this;
        let id = e.currentTarget.id;
        let url = util.fillUrlWithJson(detailUrl, { 'id': id });
        wx.navigateTo({
            url: url
        })
    },
    toLocation: function (e) {
        let index = e.currentTarget.dataset.index;
        let location = this.data.dataList[index].location;
        let address = this.data.dataList[index].address;
        wx.openLocation({
            latitude: location.lat,
            longitude: location.lng,
            scale: 28,
            address: address
        })
    }
})

/**
 * TODO 这样做效率高些，但是直接修改了dataList本身，如果这个组件中途失败那dataList就不保，将dataList深拷贝效率有比较低...;没实现解耦，默认有？默认没有？特殊字段处理？
 */
function fillData(dataList) {
    dataList.forEach(function (e) {
        if (e.name == null) { e.name = '暂无'; }
        if (e.address == null) { e.address = ''; }
        if (e.companyType == null) { e.companyType = '暂无'; }
        if (e.dutyPerson == null) { e.dutyPerson = '暂无'; }
        if (e.registerMoney == null) { e.registerMoney = '暂无'; } else { e.registerMoney = e.registerMoney + '万人民币' }
        if (e.registerAuthority == null) { e.registerAuthority = '暂无'; }
        addImageUrlForData(e);
    })
    return dataList;
}

function addImageUrlForData(data) {
    let location = data.location;
    let imageUrl = util.fillUrlWithJson(baidu_static_url, {
        'ak': 'n66HGEMQ0294ULbZf6VrZliALG2eDG4N',
        'center': location.lng + ',' + location.lat,
        'width': '340',
        'height': '250',
        'zoom': '16',
        'scale': '2',
        'copyright': '16',
        'markers': location.lng + ',' + location.lat
    });
    data.imageUrl = imageUrl;
}