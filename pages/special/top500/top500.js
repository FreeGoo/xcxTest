var app = getApp();
var util = require('../../../utils/util.js');
const path = '/top500';
const detailUrl = '../../comDetails/comDetails';

Page({
    data: {
        dataList: [
            // {
            //     id: '',
            //     name: ''
            // }
        ],
        moduleList: [
            {
                region: 'bohai',
                name: '环渤海经济带',
                select: true,
                style: 'blue',
                triangle_style: 'triangle-down-blue'
            }, {
                region: 'changjiang',
                name: '长江三角经济带',
                select: false,
                style: 'orange',
                triangle_style: 'triangle-down-orange'
            }, {
                region: 'zhujiang',
                name: '珠江三角经济带',
                select: false,
                style: 'green',
                triangle_style: 'triangle-down-green'
            }
        ],
        hidden: true
    },
    onReady: function () {
        this.fetchData(0);
    },
    fetchData: function (region) {
        let that = this;
        if (region === 0) region = 'bohai';
        let url = app.domain() + path;
        wx.request({
            url: url,
            data: { 'region': region },
            method: 'GET',
            success: function (res) {
                console.log(res);
                let dataList = res.data.data;
                dataList = fillData(dataList);
                that.setData({
                    dataList: dataList,
                    hidden: false
                });
                console.log(that.data.dataList);
            },
            fail: function (e) {
                console.log(e);
                // fail
            },
        })
    },
    switchModule: function (e) {
        let that = this;
        let region = e.currentTarget.dataset.region;
        let index = e.currentTarget.id;
        let moduleList = that.changeSelect(that.data.moduleList, index);
        that.setData({
            moduleList: moduleList,
            hidden: true
        });
        that.fetchData(region);
    },
    changeSelect: function (list, index) {
        for (let i = 0; i < list.length; i++) {
            let temp = list[i];
            temp.select = (i == index) ? true : false;
            list[i] = temp;
        }
        return list;
    },
    toDetail: function (e) {
        console.log(e);
        let that = this;
        let id = e.currentTarget.id;
        let url = util.fillUrlWithJson(detailUrl, { 'id': id });
        wx.navigateTo({
            url: url
        })
    }
})

function fillData(dataList) {
    dataList.forEach(function (e) {
        if (e.name == null) { e.name = '暂无'; }
    })
    return dataList;
}