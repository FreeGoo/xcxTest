var app = getApp();
var util = require('../../../utils/util.js');
const detailPath = '../webstar/webstar';

Page({
    data: {
        dataList: [

        ],
        hidden: false,
        moduleList: [
            {
                color: 'light-orange',
                name: '颜值高'
            }, {
                color: 'light-green',
                name: '段子手'
            }, {
                color: 'blue',
                name: '超有才'
            }, {
                color: 'light-blue',
                name: '时尚达人'
            }, {
                color: 'orange',
                name: '炒作'
            }, {
                color: 'purpe',
                name: '视频红人'
            }, {
                color: 'pink',
                name: '直播能手'
            }, {
                color: 'green',
                name: '励志青年'
            }, {
                color: 'yellow',
                name: '其他'
            }
        ]
    },
    toDetail: function (e) {
        console.log(e);
        let id = e.currentTarget.id;
        let title = this.data.moduleList[id].name;
        let ptype = +id + 1;
        let url = util.fillUrlWithJson(detailPath, {
            'title': title,
            'ptype': ptype
        });
        wx.setStorage({
            key: 'title',
            data: title
        });
        wx.setStorage({
            key: 'ptype',
            data: ptype
        })
        wx.redirectTo({
          url: url
        })
    }
})