var util = require('../../../utils/util.js');
var app = getApp();
const peopleUrl = '../starDetail/starDetail';
const documentUrl = '../star/star';

Page({
    data: {
        dataList: [
            // {
            //     id: '',
            //     name: ''
            // }
        ],
        title: '',
        percent: ''
    },
    onLoad: function (option) {
        let title = option.title;
        if (title == null || title == ''|| title == undefined) {title = wx.getStorageSync('title');}
        title = title + '网红';
        
        let ptype = option.ptype;
        if (ptype == null || ptype == ''|| title == undefined) {ptype = wx.getStorageSync('ptype');}
        this.setData({
            title: title
        })
        this.fetchData(ptype);
    },
    fetchData: function (ptype) {
        let that = this;
        let url = app.domain() + '/webstar/vote';
        ptype = parseInt(ptype);
        let data = util.jsonToForm({ 'type': ptype });
        wx.request({
            url: url,
            header: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: data,
            method: 'POST',
            success: function (res) {
                console.log(res);
                let data = res.data.data;
                let percent = data.percent;
                let dataList = data.webStars;
                percent = util.roundByDigit(+percent * 100, 1) + '%';
                that.setData({
                    percent: percent,
                    dataList: dataList
                });
                console.log(that.data.dataList);
            }
        })
    },
    toPeople: function (e) {
        let id = e.currentTarget.id; 
        let url = util.fillUrlWithJson(peopleUrl,{
            'id': id, 
            'fromPath': '/webstar-detail' 
        });
        wx.navigateTo({
            url: url
        })
    },
    toDocument: function (e) {
        console.log(e);
        let url = util.fillUrlWithJson(documentUrl,{
            'title': '网红列表',
            'url': app.domain() + '/webstar/list'
        });
        wx.navigateTo({
            url: url
        })
    },

})