function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

/**
 *  与request的Get方法一样，填充完成拼接。 url与{type: 1,id: 2} => url?type=1&id=2 
 */
function fillUrlWithJson(url, data) {
  let params = jsonToForm(data);
  return url + '?' + params;
}

/**
 * {'a':'1','b':'2'}  => 'a=1&b=2' 
 */
function jsonToForm(json) {
  let str = [];
  for (let p in json) {
    str.push(p + "=" + json[p]);
  }
  return str.join("&");
}

/**
 * 按位四舍五入
 */
function roundByDigit(numberRound, roundDigit) {
  if (numberRound >= 0) {
    let tempNumber = parseInt((numberRound * Math.pow(10, roundDigit) + 0.5)) / Math.pow(10, roundDigit);
    return tempNumber.toFixed(roundDigit);
  } else {
    numberRound1 = -numberRound;
    let tempNumber = parseInt((numberRound1 * Math.pow(10, roundDigit) + 0.5)) / Math.pow(10, roundDigit);
    return -tempNumber;
  }
}

/**
 * @description: 自定义过滤器-日期格式化
 * @author: lwk
 * @usage: v-text=""
 */

function formatDate(value, pattern) {

  pattern = pattern || 'yyyy-MM-dd';

  if (value instanceof Date === false && !/^[1-9]*[1-9][0-9]*$/.test(value)) {return value}
  if (typeof pattern !== 'string') {return value}

  if (/^[1-9]*[1-9][0-9]*$/.test(value)) {
    value = new Date(parseInt(value));
  }

  const obj = {
    'M+': value.getMonth() + 1, // 月份
    'd+': value.getDate(), // 日
    'h+': value.getHours(), // 小时
    'm+': value.getMinutes(), // 分
    's+': value.getSeconds(), // 秒
    'q+': Math.floor((value.getMonth() + 3) / 3), // 季度
    'S': value.getMilliseconds() // 毫秒
  };
  let temp;

  if (/(y+)/.test(pattern)) {
    pattern = pattern.replace(RegExp.$1, (value.getFullYear() + '').substr(4 - RegExp.$1.length));
  }

  for (const key in obj) {
    if (new RegExp('(' + key + ')').test(pattern) && RegExp.$1.length === 1) {
      temp = obj[key];
    } else {
      temp = ('00' + obj[key]).substr(('' + obj[key]).length);
    }
    pattern = pattern.replace(RegExp.$1, temp);
  }

  return pattern;

}

module.exports = {
  formatTime: formatTime,
  fillUrlWithJson: fillUrlWithJson,
  jsonToForm: jsonToForm,
  roundByDigit: roundByDigit,
  formatDate:formatDate
}
